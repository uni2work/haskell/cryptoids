{ ghc, nixpkgs ? (import <nixpkgs> {}) }:

let
  inherit (nixpkgs) haskell pkgs;
in haskell.lib.buildStackProject {
  inherit ghc;
  name = ''stackenv-cryptoids'';
  buildInputs = with pkgs;
    [];
}
