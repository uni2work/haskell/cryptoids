module Data.CryptoID
  ( CryptoID(..)
  ) where

import GHC.Generics (Generic)
import Data.Typeable (Typeable)
import Data.Data (Data)
import GHC.TypeLits (Symbol)

import Data.Binary (Binary)
import Foreign.Storable (Storable)

import Web.PathPieces (PathPiece)
import Web.HttpApiData (ToHttpApiData, FromHttpApiData)

import Control.DeepSeq (NFData)
import Data.Aeson (ToJSON, ToJSONKey, FromJSON, FromJSONKey)

import Data.Hashable (Hashable)

newtype CryptoID (namespace :: Symbol) a = CryptoID { ciphertext :: a }
  deriving ( Eq, Ord
           , Read, Show
           , Binary, Storable, NFData, Hashable
           , Data, Typeable, Generic
           , PathPiece, ToHttpApiData, FromHttpApiData
           , ToJSON, ToJSONKey, FromJSON, FromJSONKey
           )
