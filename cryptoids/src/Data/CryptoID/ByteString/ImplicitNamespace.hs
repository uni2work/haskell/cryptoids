{-|
Description: Encryption of bytestrings with implicit type level nonces
License: BSD3
-}
module Data.CryptoID.ByteString.ImplicitNamespace
  ( CryptoByteString
  , HasCryptoByteString
  , module Data.CryptoID.ByteString
  , module Data.CryptoID.Class.ImplicitNamespace
  ) where

import Data.CryptoID.Class.ImplicitNamespace

import Data.CryptoID.ByteString hiding (encrypt, decrypt, CryptoID, HasCryptoID, CryptoByteString, HasCryptoByteString)

import Data.ByteString (ByteString)

type CryptoByteString plaintext = CryptoID ByteString plaintext
type HasCryptoByteString plaintext = HasCryptoID ByteString plaintext
