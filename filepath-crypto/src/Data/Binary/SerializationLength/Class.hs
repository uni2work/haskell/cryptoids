{-# LANGUAGE TypeFamilies #-}

module Data.Binary.SerializationLength.Class
  ( HasFixedSerializationLength(..)
  ) where

import GHC.TypeLits

-- | The class of types for which the result of serialization with @Data.Binary@
--   is known statically to be of a certain length
class KnownNat (SerializationLength a) => HasFixedSerializationLength a where
  -- | The 'SerializationLength' is given in bytes at type level
  type SerializationLength a :: Nat
