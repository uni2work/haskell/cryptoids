# 0.1.0.0

    - Add support for 'cryptoids-class'

# 0.0.0.3

    - Got rid of `encoding`

# 0.0.0.2

    - Improved documentation

# 0.0.0.1

    - Improved documentation

# 0.0.0.0

First published version
