# 0.5.1.0

    - Add 'CiphertextIsWrongLength'
    - Bump version bound on 'cryptonite'

# 0.5.0.0

    - Add support for 'cryptoids-class'

# 0.4.0.0

    - Expose 'cipherBlockSize'
    - Adjust 'Data.CryptoID.Poly' to allow for more dynamic padding

# 0.3.0.0

    - Better exception type (does no longer leak private information)
    - 'Data.CryptoID.Poly' now supports padding the plaintext to a certain length before encryption

# 0.2.0.0

    - Rename 'Data.CryptoID.Poly' to 'Data.CryptoID.ByteString'
    - Introduce 'Data.CryptoID.Poly' doing actual serialization

# 0.1.0.1

    - Correct mistakes in the documentation

# 0.1.0

    - Switch to using 'MonadThrow' instead of 'MonadError'
    - Introduce 'readKeyFile'

# 0.0.0

First published version

