argumentPackages@{ ... }:

let
  defaultPackages = (import <nixpkgs> {}).haskellPackages;
  haskellPackages = defaultPackages // argumentPackages;
in import ./cryptoids.nix { inherit (haskellPackages) callPackage; }
