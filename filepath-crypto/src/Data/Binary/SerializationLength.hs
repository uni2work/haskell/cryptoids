{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TemplateHaskell #-}

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Data.Binary.SerializationLength
  ( HasFixedSerializationLength(..)
  ) where

import Data.Binary.SerializationLength.Class
import Data.Binary.SerializationLength.TH

import Data.Int
import Data.Word

$(hasFixedSerializationLength ''Word8 1)
$(hasFixedSerializationLength ''Word16 2)
$(hasFixedSerializationLength ''Word32 4)
$(hasFixedSerializationLength ''Word64 8)

$(hasFixedSerializationLength ''Int8 1)
$(hasFixedSerializationLength ''Int16 2)
$(hasFixedSerializationLength ''Int32 4)
$(hasFixedSerializationLength ''Int64 8)
