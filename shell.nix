{ nixpkgs ? import <nixpkgs> {}, compiler ? null }:

let
  inherit (nixpkgs) pkgs;

  haskellPackages = if isNull compiler
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  drvs = import ./. { inherit (haskellPackages) callPackage; };

  override = oldAttrs: {
    nativeBuildInputs = oldAttrs.nativeBuildInputs ++ (with pkgs; [ cabal2nix gup ]) ++ (with haskellPackages; [ hlint stack cabal-install ]);
    shellHook = ''
      ${oldAttrs.shellHook}
      export PROMPT_INFO="${oldAttrs.name}"
    '';
  };
in
  pkgs.lib.mapAttrs (name: drv: pkgs.stdenv.lib.overrideDerivation drv.env override) drvs
