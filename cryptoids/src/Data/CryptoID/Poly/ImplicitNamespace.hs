{-|
Description: Encryption of serializable values with implicit type level nonces
License: BSD3
-}
module Data.CryptoID.Poly.ImplicitNamespace
  ( module Data.CryptoID.Poly
  , module Data.CryptoID.Class.ImplicitNamespace
  ) where

import Data.CryptoID.Class.ImplicitNamespace

import Data.CryptoID.Poly hiding (encrypt, decrypt, CryptoID, HasCryptoID)
