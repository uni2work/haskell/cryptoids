{-|
Description: Typeclass based interface to 'cryptoids'
License: BSD3

Polymorphic functions to perform cryptographic operations on 'CryptoID's in a monadic context
-}
module Data.CryptoID.Class
  ( MonadCrypto(..)
  , HasCryptoID(..)
  ) where

import Data.CryptoID (CryptoID)

import GHC.TypeLits (Symbol)

import Control.Monad.Catch (MonadThrow)

-- | Class of monads granting reader access to a key and allowing for failure during cryptographic operations
--
-- This formulation is weaker than @MonadReader key@ (from mtl) in that it does not require @local@.
class MonadThrow m => MonadCrypto (m :: * -> *) where
  type MonadCryptoKey m :: *
  cryptoIDKey :: (MonadCryptoKey m -> m a) -> m a

-- | Multiparameter typeclass of @(namespace, ciphertext, plaintext, monad)@ tuples which allow for cryptographic operations on 'CryptoID's with appropriate @namespace@, @plaintext@, and @ciphertext@, utilising the state of @monad@
--
-- Instances of this typeclass are usually universally quantified over (at least) @namespace@, and @m@
class MonadCrypto m => HasCryptoID (namespace :: Symbol) (ciphertext :: *) (plaintext :: *) (m :: * -> *) where
  encrypt :: plaintext -> m (CryptoID namespace ciphertext)
  -- ^ Encrypt a @plaintext@ in a fashion dependent on the @namespace@ and desired @ciphertext@-type retrieving the key from and throwing errors into @m@
    
  decrypt :: CryptoID namespace ciphertext -> m plaintext
  -- ^ Encrypt a @ciphertext@ in a fashion dependent on the @namespace@ and desired @plaintext@-type retrieving the key from and throwing errors into @m@


