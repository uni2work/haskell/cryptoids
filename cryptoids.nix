{ callPackage }:

rec {
  cryptoids-types = callPackage ./cryptoids-types/cryptoids-types.nix {};
  uuid-crypto = callPackage ./uuid-crypto/uuid-crypto.nix {};
  filepath-crypto = callPackage ./filepath-crypto/filepath-crypto.nix {};
  cryptoids = callPackage ./cryptoids/cryptoids.nix { inherit cryptoids-types; };
}
