{-|
Description: Reversably generate filepaths from arbitrary serializable types with implicit type level nonces
License: BSD3
-}
module System.FilePath.Cryptographic.ImplicitNamespace
  ( CryptoFileName
  , HasCryptoFileName
  , module System.FilePath.Cryptographic
  , module Data.CryptoID.Class.ImplicitNamespace
  ) where

import Data.CryptoID.Class.ImplicitNamespace

import System.FilePath.Cryptographic hiding (encrypt, decrypt, CryptoID, HasCryptoID, CryptoFileName, HasCryptoFileName)

import System.FilePath (FilePath)
import Data.CaseInsensitive (CI)

type CryptoFileName plaintext = CryptoID (CI FilePath) plaintext
type HasCryptoFileName plaintext = HasCryptoID (CI FilePath) plaintext
