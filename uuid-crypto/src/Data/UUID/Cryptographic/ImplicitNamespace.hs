{-|
Description: Reversably generate UUIDs from arbitrary serializable types with implicit type level namespaces
License: BSD3
-}
module Data.UUID.Cryptographic.ImplicitNamespace
  ( CryptoUUID
  , HasCryptoUUID
  , module Data.UUID.Cryptographic
  , module Data.CryptoID.Class.ImplicitNamespace
  ) where

import Data.CryptoID.Class.ImplicitNamespace

import Data.UUID.Cryptographic hiding (encrypt, decrypt, CryptoID, HasCryptoID, CryptoUUID, HasCryptoUUID)

import Data.UUID (UUID)

type CryptoUUID plaintext = CryptoID UUID plaintext
type HasCryptoUUID plaintext = HasCryptoID UUID plaintext
