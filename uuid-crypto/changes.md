# 1.4.0.0

    - Add support for 'cryptoids-class'

# 1.3.1.0

    - Fix documentation mistake
    - Bump @cryptoids@ to @0.4.0.*@

# 1.3.0.1

    - Fix documentation typo

# 1.3.0.0

    - Fix decryption

# 1.2.0.0

    - Pad plaintext before encryption, allowing encryption of payloads shorter than 128 bits

# 1.1.1.0

    - Switch to using the new 'Data.CryptoID.Poly'

# 1.1.0.1

    - Update version constraint on @cryptoids@

# 1.1.0

    - Switch to using 'MonadThrow' instead of 'MonadError'

# 1.0.0

First published version
